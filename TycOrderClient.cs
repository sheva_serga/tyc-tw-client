﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using TYC.TWOrderClient.Common;
using TYC.TWOrderClient.Models;

namespace TYC.TWOrderClient
{
    public class TycOrderClient : ITycOrderClient
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        private readonly TycOrderClientSettings _settings;

        public TycOrderClient(TycOrderClientSettings settings)
        {
            _settings = settings;
        }

        public async Task<ActionResult> UpdateOrderStatusAsync(TitleWaveOrderStatus orderStatus)
        {
            String url = $"https://{_settings.TYCBaseUrl}/api/twintegration/updateorderstatus";

            ObjectContent<TitleWaveOrderStatus> content = new ObjectContent<TitleWaveOrderStatus>(
                      orderStatus, new JsonMediaTypeFormatter()
                );

            content.Headers.Add("XXX-API-KEY", _settings.ApiKey);

            HttpResponseMessage message = await _httpClient.PutAsync(url, content);

            return await message.Content.ReadAsAsync<ActionResult>();
        }

        public async Task<ActionResult> UploadDocumentAsync(TitleWaveDocument document)
        {
            String url = $"https://{_settings.TYCBaseUrl}/api/twintegration/pushdocument";

            ObjectContent<TitleWaveDocument> content = new ObjectContent<TitleWaveDocument>(
                      document, new BsonMediaTypeFormatter()
                );

            content.Headers.Add("xxx-api-key", _settings.ApiKey);
            content.Headers.TryAddWithoutValidation("Accept", "application/json");

            HttpResponseMessage message = await _httpClient.PostAsync(url, content);

            return await message.Content.ReadAsAsync<ActionResult>(new MediaTypeFormatter[] { new JsonMediaTypeFormatter() });
        }

        public async Task<ActionResult> UploadXmlDataAsync(TitleWaveDocumentXml documentXml)
        {
            String url = $"https://{_settings.TYCBaseUrl}/api/twintegration/pushdocumentxmlfile";

            ObjectContent<TitleWaveDocumentXml> content = new ObjectContent<TitleWaveDocumentXml>(
                      documentXml, new BsonMediaTypeFormatter()
                );

            content.Headers.Add("XXX-API-KEY", _settings.ApiKey);
            content.Headers.TryAddWithoutValidation("Accept", "application/json");

            HttpResponseMessage message = await _httpClient.PostAsync(url, content);

            return await message.Content.ReadAsAsync<ActionResult>();
        }

        public async Task<ActionResult> UploadOrderNoteAsync(TitleWaveNote note)
        {
            String url = $"https://{_settings.TYCBaseUrl}/api/twintegration/pushordernote";

            ObjectContent<TitleWaveNote> content = new ObjectContent<TitleWaveNote>(note, new JsonMediaTypeFormatter());
            content.Headers.Add("XXX-API-KEY", _settings.ApiKey);

            HttpResponseMessage message = await _httpClient.PostAsync(url, content);

            return await message.Content.ReadAsAsync<ActionResult>();
        }


        public async Task<ActionResult> SendOrderCreationResultAsync(TitleWaveOrderCreationResult result)
        {
            String url = $"https://{_settings.TYCBaseUrl}/api/twintegration/receiveordernumber";

            ObjectContent<TitleWaveOrderCreationResult> content = new ObjectContent<TitleWaveOrderCreationResult>(
                      result
                    , new JsonMediaTypeFormatter()
                );

            content.Headers.Add("XXX-API-KEY", _settings.ApiKey);

            HttpResponseMessage message = await _httpClient.PostAsync(url, content);

            return await message.Content.ReadAsAsync<ActionResult>();
        }
    }
}
