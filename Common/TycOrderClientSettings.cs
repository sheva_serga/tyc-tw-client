﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Common
{
    public class TycOrderClientSettings
    {
        public String TYCBaseUrl { get; set; }
        public String ApiKey { get; set; }
    }
}
