﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Common
{
    public enum OrderStatus
    {
        Pending = 1,
        Active = 2,
        Declined = 3,
        Cancelled = 4,
        Completed = 5,
        HandOver = 6,
        Deleted = 8
    }
}
