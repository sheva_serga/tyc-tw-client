﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Common
{
    public class ActionResult
    {
        public ActionStatus Status { get; set; }
        public String Message { get; set; }
    }
}
