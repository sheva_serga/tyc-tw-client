﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Models
{
    public class TitleWaveNote
    {
        public String ClientOrderNumber { get; set; } //required
        public String TitleWaveOrderNumber { get; set; } //required
        public String Content { get; set; } //required
    }
}
