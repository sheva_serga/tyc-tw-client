﻿using System;

namespace TYC.TWOrderClient.Models
{
    public class TitleWaveDocumentXml
    {
        public String ClientOrderNumber { get; set; } //required
        public String TitleWaveOrderNumber { get; set; }
        public byte[] XmlContent { get; set; } //required
    }
}