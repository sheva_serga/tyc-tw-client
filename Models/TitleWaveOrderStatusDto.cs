﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.TWOrderClient.Common;

namespace TYC.TWOrderClient.Models
{
    public class TitleWaveOrderStatus
    {
        public String ClientOrderNumber { get; set; } //required
        public String TitleWaveOrderNumber { get; set; } //required
        public OrderStatus Status { get; set; } //required
    }
}
