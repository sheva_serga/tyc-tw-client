﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Models
{
    public class TitleWaveOrderCreationResult
    {
        public String ClientOrderNumber { get; set; } //required
        public String TitleWaveOrderNubmer { get; set; } //required
        public String Status { get; set; } //required
        public String Message { get; set; }
    }
}
