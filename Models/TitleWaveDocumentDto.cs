﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.TWOrderClient.Models
{
    public class TitleWaveDocument
    {
        public String ClientOrderNumber { get; set; } //required
        public String TitleWaveOrderNumber { get; set; } //required
        public String FileNameWithExtention { get; set; } //required
        public String DocumentName { get; set; }
        public byte[] Content { get; set; } //required
    }
}
