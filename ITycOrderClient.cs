﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.TWOrderClient.Common;
using TYC.TWOrderClient.Models;

namespace TYC.TWOrderClient
{
    public interface ITycOrderClient
    {
        Task<ActionResult> UploadDocumentAsync(TitleWaveDocument document);
        Task<ActionResult> UploadOrderNoteAsync(TitleWaveNote note);
        Task<ActionResult> UpdateOrderStatusAsync(TitleWaveOrderStatus orderStatus);
        Task<ActionResult> UploadXmlDataAsync(TitleWaveDocumentXml documentXml);
        Task<ActionResult> SendOrderCreationResultAsync(TitleWaveOrderCreationResult result);
    }
}
